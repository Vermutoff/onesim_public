<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


//Route::get('/', function () {
//    return view('welcome');
//});

Route::group(
    [
        'namespace' => 'App\Http\Controllers',
//        'as'        =>  'frontend.',
    ],
    function () {
Route::group(
    [
        'namespace' => 'Frontend',
        'as'        =>  'frontend.',
    ],
    function () {

        Route::group([
            'as' => 'pages.',
        ],
            function () {
                Route::get('/',             'IndexController@index')->name('index');
                Route::get('/clear','IndexController@clear')->name('clear');
            });

    });
    });
