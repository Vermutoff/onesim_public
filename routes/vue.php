<?php

use Illuminate\Support\Facades\Route;


Route::group(
    [
        'namespace' => 'App\Http\Controllers',

    ],
    function () {

        Route::group(
            [
                'namespace' => 'Vue',
                'as'        =>  'vue.',
            ],
            function() {

                Route::resource('owners', 'OwnerController');

                Route::post('search', 'SearchController@handle');
            });
    });

