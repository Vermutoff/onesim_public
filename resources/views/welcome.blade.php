<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">

        <!-- Styles -->
        <link rel="stylesheet" href="{{ asset('css/app.css') }}" />


        <style>
            body {
                font-family: 'Nunito';
            }
        </style>
    </head>
    <body  class="antialiased">
        <div id="app" class="pt-2 pb-2 ">
            <div class="container">
                <div class="row">
                    <div class="col-md-8">
                        <owners-list :ownersarray="{{ json_encode($owners->items()) }}"></owners-list>

{{--                        {{ $owners->links() }}--}}
                    </div>
                </div>

            </div>

        </div>
    </body>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>
</html>
