require('./bootstrap');

window.Vue = require('vue');
window.Event = new Vue();


Vue.component('owners-list',        require('./components/owners/list.vue').default);

const app = new Vue({
    el: '#app',
});
