<?php

namespace App\Contracts\Services;


interface SearchServiceInterface
{

    public function handle(array $params);

}
