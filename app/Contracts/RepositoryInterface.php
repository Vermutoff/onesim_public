<?php
namespace App\Contracts;


interface RepositoryInterface
{

    public function all(array $related);

    public function paginate(int $paginate);

    public function firstOrFail(array $where, $related=null);

    public function first(array $where);

    public function findOrFail(int $id, $related=null);

    public function find(int $id);

    public function last($related=null, $paginated=null, $take=null);

    public function getWhere($where, $related=null, $paginated=null, $take=null);

}
