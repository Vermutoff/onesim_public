<?php
namespace App\Contracts;


interface BaseServiceInterface
{

    public function repository(string $queryName, array $params=[]);

    public function get(string $queryName, array $params=[]);

    public function create(array $array);

    public function update(array $array, int $id);

    public function delete(int $id);

    public function deleteModel($model);

    public function updateModel(array $requestArray, $model);

}
