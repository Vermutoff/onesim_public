<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
//use Illuminate\Support\Facades\Gate;
use Illuminate\Http\Exceptions\HttpResponseException;
use \Illuminate\Contracts\Validation\Validator;

class PostSearchRequest extends FormRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'type'  =>  ['required','string'],
            'text'  =>  [
                'required',
                'string',
                'min:3',
                'max:50',
                'regex:/^[a-zA-Z0-9а-яА-Я ]+$/iu'
            ],
        ];
    }

    public function messages()
    {
        return [
            'type.required' =>  'Неизвестный тип',
            'type.string'   =>  'Неизвестный тип',
            'text.required' =>  'Пожалуйста, введите поисковый запрос',
            'text.min'      =>  'Пожалуйста, введите более 3 символов',
            'text.max'      =>  'Пожалуйста, введите менее 50 символов',
            'text.regex'    =>  'Пожалуйста, используйте только буквы и цифры',
        ];
    }

    protected function failedValidation(Validator $validator)
    {

        throw new HttpResponseException(response()->json([
            'result'    =>  false,
            'return'    =>  null,
            'errors'    =>  $this->getErrorsList($validator->messages()->toArray()),
            'message'   =>  'Ошибка валидации запроса'
        ])); // , 422
    }


    // Преобразуем все ошибки в единый плоский массив,
    // который отобразим на фронте
    public function getErrorsList($validatorErrorsArray){
        return \Arr::collapse($validatorErrorsArray);
    }

}
