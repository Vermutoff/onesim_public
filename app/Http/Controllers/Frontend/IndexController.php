<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;
use App\Http\Controllers\Controller;

class IndexController extends Controller
{

    public function index(){

        return view('welcome');
    }

    public function clear() {
        Artisan::call('config:clear');
        Artisan::call('cache:clear');

        Artisan::call('view:clear');
        Artisan::call('route:clear');

        Artisan::call('optimize');
//        composer dump-autoload -o
//         Artisan::call('backup:clean');
        Artisan::call('config:clear --env=testing');
        Artisan::call('config:clear');
        return "Кэш очищен.";
    }

}
