<?php

namespace App\Http\Controllers\Vue;

use App\Contracts\Services\SearchServiceInterface;
use App\Http\Controllers\Controller;
use App\Http\Requests\PostSearchRequest;

class SearchController extends Controller
{
    protected $service;

    public function __construct(SearchServiceInterface $service){
        $this->service = $service;
    }


    /***
     * Единственный метод в контроллере, который отвечает за старт поиска
     * @param PostSearchRequest $request
     * @return array
     */
    public function handle(PostSearchRequest $request){
        if($result = $this->service->handle($request->validated()))
            return ([
                'result'    =>  true,
                'return'    =>  $result,
                'message'   =>  'Успешно!'
            ]);
        return ([
            'result'    =>  false,
            'return'    =>  null,
            'message'   =>  'Ничего не найдено'
        ]);
    }
}
