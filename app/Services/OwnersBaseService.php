<?php
namespace App\Services;

use \App\Contracts\RepositoryInterface;

class OwnersBaseService extends AbstractBaseService
{

    protected $repository;

    public function __construct(RepositoryInterface $repository){
        $this->repository = $repository;
    }


}
