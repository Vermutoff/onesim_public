<?php
namespace App\Services;

use App\Contracts\BaseServiceInterface;

class AbstractBaseService implements BaseServiceInterface
{

    protected $repository;


    //  Обращаемся непосредственно к репозиторию текущего сервиса
    //  и получаем необходимые данные
    public function repository(string $queryName, array $params=[]){
        if(method_exists($this->repository, $queryName))
        {
            return call_user_func_array([$this->repository, $queryName], $params);
        }
        return false;
    }


    //  Вызывает метод repository
    public function get(string $queryName, array $params=[]){
        return $this->repository($queryName, $params);
    }




    public function create(array $array){
        return $this->repository->add($array);
    }


    public function update(array $array, int $id){
        return $this->repository->update($array, $id);
    }


    public function updateModel(array $requestArray, $model)
    {
        $model->update($requestArray);
        return $this->repository->findOrFail($model->id);
    }


    public function delete(int $id){
        return $this->repository->delete($id);
    }


    public function deleteModel($model)
    {
        return $model->delete();
    }


}
