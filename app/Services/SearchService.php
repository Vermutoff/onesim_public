<?php
namespace App\Services;

use App\Contracts\Services\SearchServiceInterface;

class SearchService implements SearchServiceInterface
{

    /***
     * Единственный метод в сервисе, который отвечает за старт поиска
     * @param array $params
     * @return mixed
     */
    public function handle(array $params)
    {

        //  Получаем массив слов из строки поискового запроса
        $textArray = explode(' ', $params['text']);

        //  Перебираем массив слов и удаляем пробелы по краям
        foreach ($textArray as $word) $newTextArray[] = trim($word);

        //  Из слов формируем массив строк,
        //  которые содержат регулярные выражения для последующего поиска
        $regexpArray=[];
        foreach ($newTextArray as $newText) $regexpArray[]="(.*".$newText.".*)";

        // Объявляем массив столбцов, по которым будет производится поиск
        $columns=[
            'firstname',
            'lastname',
            'phone',
            'company',
            'country'
        ];

        // Начинаем формировать строку запроса в БД
        $q = \DB::table('owners');

        //  Добавляем параметры where в строку запроса
        $q->where(function($query) use ($regexpArray, $columns) {
            // Перебираем массив с выражениями и добавляем параметр where в строку запроса
            foreach ($regexpArray as $regexp) {
                $query->where(function ($query) use ($regexp,$columns) {
                    // Перебираем массив столбцов и добавляем параметр where в строку запроса
                    foreach($columns as $column) {
                        $query->orWhere($column, 'REGEXP', $regexp);
                    }
                });
            }
        });

        //  Возвращаем результат поиска в БД на основе подготовленого запроса
        return $q->get();
    }


}
