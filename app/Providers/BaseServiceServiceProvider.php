<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

use Illuminate\Container\Container;

class BaseServiceServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $container = Container::getInstance();

        //  Базовый сервис Овнеров
        $container
            ->when(\App\ViewComposers\OwnersComposerView::class)
            ->needs(\App\Contracts\BaseServiceInterface::class)
            ->give(\App\Services\OwnersBaseService::class);

    }


}
