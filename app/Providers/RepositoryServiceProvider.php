<?php

namespace App\Providers;

use Illuminate\Container\Container;
use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $container = Container::getInstance();

        //  Отдаем реализацию Репозитория Овнеров
        $container
            ->when(\App\Services\OwnersBaseService::class)
            ->needs(\App\Contracts\RepositoryInterface::class)
            ->give(\App\Repositories\OwnersRepository::class);


    }


}
