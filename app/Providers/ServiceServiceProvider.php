<?php

namespace App\Providers;

use App\Contracts\Services\SearchServiceInterface;
use App\Services\SearchService;
use Illuminate\Support\ServiceProvider;

class ServiceServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //  Синглтон для поискового сервиса
        $this->app->singleton(SearchServiceInterface::class, SearchService::class);
    }

}
