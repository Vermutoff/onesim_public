<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\View;
//use Illuminate\Contracts\View\View;

class ViewServiceProvider extends ServiceProvider
{


    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //  Для базового шаблона используем композер
        View::composer(
            'welcome', 'App\ViewComposers\OwnersComposerView'
        );

    }
}
