<?php

namespace App\ViewComposers;

use App\Contracts\BaseServiceInterface;
use Illuminate\Contracts\View\View;

class OwnersComposerView
{
    protected $service;

    public function __construct(BaseServiceInterface $service)
    {
        $this->service = $service;
    }



    public function compose(View $view)
    {
        $owners = $this->service->get('paginate', [48]);

        $view->with([
            'owners'        => $owners,
        ]);
    }
}
