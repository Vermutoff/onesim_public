<?php

namespace App\Repositories;

use App\Contracts\RepositoryInterface;

class AbstractRepsitory implements RepositoryInterface
{

    protected $model, $related;


    /***
     * Возвращаем все данные из БД
     * @param array $related
     * @return mixed
     */
    public function all(array $related=[])
    {
        return $this->model->with($related)->get();
    }

    /***
     * @param int $paginated
     * @param array $related
     * @return mixed
     */
    public function paginate(int $paginated=15, array $related=[])
    {
        if(empty($related)) $related = $this->related;
        return $this->model->with($related)->paginate($paginated);
    }

    /***
     * @param int $id
     * @param null $related
     * @return mixed
     */
    public function findOrFail(int $id, $related=null)
    {
        if(empty($related)) $related=$this->related;
        return $this->model->with($related)->findOrFail($id);
    }

    /***
     * @param int $id
     * @param null $related
     * @return mixed
     */
    public function find(int $id, $related=null)
    {
        return $this->findOrFail($id, $related);
    }

    /***
     * @param array $where
     * @param null $related
     * @return mixed
     */
    public function firstOrFail(array $where, $related=null)
    {
        if(empty($related)) $related=$this->related;
        return $this->model->with($related)->firstOrFail($where);
    }

    /***
     * Первый, подходящий по параметрам where
     * @param array $where
     * @param null $related
     * @return mixed
     */
    public function first(array $where, $related=null)
    {
        return $this->firstOrFail($where, $related=null);
    }


    /***
     * Последние с отношениями и сортировкой
     * @param null $related
     * @param null $paginated
     * @param null $take
     * @return mixed
     */
    public function last($related=null, $paginated=null, $take=null){
        if(empty($related)) $related=$this->related;

        if($paginated) return  $this->model->with($related)->orderBy('id','desc')->paginate($paginated);
        if($take) return  $this->model->with($related)->orderBy('id','desc')->take($take);
        return  $this->model->with($related)->orderBy('id','desc')->get();
    }


    /***
     * @param $where
     * @param null $related
     * @param null $paginated
     * @param null $take
     * @return mixed
     */
    public function getWhere($where, $related=null, $paginated=null, $take=null){
        if(empty($related)) $related=$this->related;

        if($paginated) return  $this->model->with($related)->where($where)->paginate($paginated);
        if($take) return  $this->model->with($related)->where($where)->take($take);
        return $this->model->with($related)->where($where)->get();
    }


    /** Базовые операции
    ===========================================*/
    public function create(array $array){
        return $this->model->create($array);
    }

    public function add(array $array){
        return $this->create($array);
    }

    public function update(array $array, $id)
    {
        $model = $this->find($id);
        $model->update($array);
        return $this->find($id);
    }


    public function deleteWhere($where){
        return $this->model->where($where)->delete();
    }

    public function delete($id){
        return $this->model->destroy($id);
    }


}
