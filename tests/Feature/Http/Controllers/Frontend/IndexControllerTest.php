<?php

namespace Tests\Feature;

use App\Models\Owner;
use Database\Seeders\OwnersTableSeeder;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class IndexControllerTest extends TestCase
{
    use RefreshDatabase;

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testIndexPageHasOwners()
    {
        $this->seedData();

        $response = $this->get('/');

        $response->assertStatus(200);
        $response->assertViewHas('owners');
    }

    public function seedData(){
        for($j = 0; $j < 1; $j++){
            $user_data =  \App\Models\Owner::factory()->count(1000)->make()->toArray();
            \DB::table('owners')->insert($user_data);
        }
    }
}
