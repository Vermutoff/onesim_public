<?php

namespace Tests\Feature;

use App\Models\Owner;
use Database\Seeders\OwnersTableSeeder;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class SearchControllerTest extends TestCase
{
    use RefreshDatabase;

    /**
     * Проверяем, что поиск успешно находит данные
     *
     * @return void
     */
    public function testSearchResultHasOwners()
    {
        $this->seedData();

        $params = [
            'type'  =>  'owners',
            'text'  => 'Николай Russia'
        ];

        $response = $this->post('/vue/search', $params);

        $response->assertJson([
            'result'    =>  true,
        ]);

        $response->assertJsonCount(1, 'return');
    }


    /**
     * Возникает ошибка в случае использования неразрешенных символов
     *
     * @return void
     */
    public function testWithDotesFailed()
    {
        $this->seedData();

        $params = [
            'type'  =>  'owners',
            'text'  => '...a'
        ];

        $response = $this->post('/vue/search', $params);

        $response->assertJson([
            'result'    =>  false,
        ]);
    }


    public function seedData(){
        $owners = [
            [
                'firstname' =>  'Иван',
                'lastname' =>  'Петров',
                'phone' =>  '+7937987716',
                'company' =>  'Mvideo',
                'country' =>  'Russia',
            ],
            [
                'firstname' =>  'Виталий',
                'lastname' =>  'Сидоров',
                'phone' =>  '+7937987716',
                'company' =>  'Eldorado',
                'country' =>  'Russia',
            ],
            [
                'firstname' =>  'Николай',
                'lastname' =>  'Ватутин',
                'phone' =>  '+7937987716',
                'company' =>  'Mvideo',
                'country' =>  'Russia',
            ]
        ];
        \DB::table('owners')->insert($owners);

        for($j = 0; $j < 1; $j++){
            $user_data =  \App\Models\Owner::factory()->count(1000)->make()->toArray();
            \DB::table('owners')->insert($user_data);
        }

    }
}
