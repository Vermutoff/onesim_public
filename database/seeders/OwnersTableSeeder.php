<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class OwnersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $owners = [
            [
                'firstname' =>  'Иван',
                'lastname' =>  'Петров',
                'phone' =>  '+7937987716',
                'company' =>  'Mvideo',
                'country' =>  'Russia',
            ],
            [
                'firstname' =>  'Виталий',
                'lastname' =>  'Сидоров',
                'phone' =>  '+7937987716',
                'company' =>  'Eldorado',
                'country' =>  'Russia',
            ],
            [
                'firstname' =>  'Николай',
                'lastname' =>  'Ватутин',
                'phone' =>  '+7937987716',
                'company' =>  'Mvideo',
                'country' =>  'Russia',
            ]
        ];
        \DB::table('owners')->insert($owners);

        // Можем управлять количеством рандомных данных,
        // меняя цифры в функции for или значение count у фабрики
        for($j = 0; $j < 100; $j++){
            $user_data =  \App\Models\Owner::factory()->count(10000)->make()->toArray();

            \DB::table('owners')->insert($user_data);
        }


    }
}
